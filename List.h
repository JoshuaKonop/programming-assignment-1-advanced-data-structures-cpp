#include "Node.h"

/*
This is the list class. Develops the singly-linked lists and all the functions necessary for the list. It is also a template,
as the list needs to be able to switch between doubles and strings, depending on if it's a terms list or a profiles list.
*/
template<class T>
class List {

public:

List() { //Constructors and destructor
    pHead = nullptr;
}
List(Node<T>* newPHead) {
    pHead = newPHead;
}
~List() {
    deleteList(pHead);
}

//Insert at front function
//Takes the new node and masterNode (head node)
//big O of O(1) 
void insert(Node<T>* newNode, Node<T>* masterNode) { 

    //Base case: if the head node is null
    if (masterNode == nullptr) {
        pHead = newNode;
    }

    else { //Otherwise
    
    newNode->setNextNode(pHead); //Set the new node to link to the head node
    pHead = newNode; //Set the root of the list to the new node
    
    }
}
//Removal function. Removes a node based off of the name (target) that is input
//MasterNode is the initializing head node, recursively advances through the list
//Big O of O(n)
void remove(string target, Node<T>* masterNode) {

    //If the head node is the target, set the head node to the next node in the list
    if (target == masterNode->getData1()) {
        pHead = pHead->getNextNode();
    }
    else if(masterNode->getNextNode()->getData1() == target) { 
        //Otherwise, if the next node is the target, make it so the current node points to the node after the target 
        masterNode->setNextNode(masterNode->getNextNode()->getNextNode());
    }
    else {
        //Lastly, if neither the current node or the next node are the target, and the next node is not null, recursively call remove() again
        if (masterNode->getNextNode() != nullptr) {
            remove(target, masterNode->getNextNode());
        }
        
    }
}

//Recursive findNode function. Returns a node if it matches the name input by the user.
//MasterNode starts as the head point and recursively advances through the list
//Big O of O(n)
Node<T>* findNode(string target, Node<T>* masterNode) {

    //If the current node matches the target, return the node
    if(masterNode->getData1() == target) {
        return masterNode;
    }
    else if (masterNode->getNextNode() != nullptr) {
    //If the current node doesn't match, recursively call findNode() looking at the next node in the list
        return findNode(target, masterNode->getNextNode());
    }
    else {
    //If the target is not found, return nullptr
        return nullptr;
    }
}

//Modified recursive function to find a certain description of a node x nodes down the list
//x is randomly determined in the game loop
//Big O of O(n)
string findNodeDesc(int target, int iterations, Node<T>* masterNode) {

    //If the amount of iterations matches the target for iterations, return the data2 from the node (specifically used solely for terms)
    if(target == iterations) {
        return masterNode->getData2();
    }
    else {
        //Otherwise, if the target doesn't match and the next node is populated, check to see if it matches the next node recursively
        if (masterNode->getNextNode() != nullptr) {
            iterations++;
            return findNodeDesc(target, iterations, masterNode->getNextNode());
        }
        else {
        //If the number cannot be found (don't think this code will ever be run), return empty
            return "";
        }
    }
}

//Delete list function
//Big O of O(n)
void deleteList(Node<T>* masterNode) {
    //If the next node isn't empty, recursively call this for the next node
    //(Will iterate through the whole list)
    if (masterNode->getNextNode() != nullptr) {
        deleteList(masterNode->getNextNode());
    }

    //Then set the node to null
    masterNode = nullptr;
}
//Prints the list out
//Big O of O(n)
//This was previously used for testing, is now used for displaying the list of names for profiles
void printList(Node<T>* cNode) {

    //cNode wasn't working properly, uses a new pCurrent instead
    Node<T>* pCurrent;
    
    if (cNode != nullptr) {
        //If the node isn't null, print the name of the node
        pCurrent = cNode;

        cout << pCurrent->getData1() << endl;

    }
   
    //Print the next node recursively
    if (cNode->getNextNode() != nullptr) {
        printList(pCurrent->getNextNode());
    }

}

//Getter
Node<T>* getHeadPointer() {
    return pHead;
}
//Get the length of the list
//Big O of O(n)
//Initial will start at 0, cNode will start at head node
int getLength(int initial, Node<T>* cNode) {

    //If the head node is null (the list is empty), return length of 0
    if (cNode == nullptr) {
        return initial;
    }
    else if (cNode->getNextNode() == nullptr) { //If the next node is null, return 1 + the current amount of initial
        initial++;
        return initial;
    }
    else { //Otherwise, iterate again recursively, increasing the initial by 1.
        initial++;
        return getLength(initial, cNode->getNextNode());
    }
}

private:

Node<T>* pHead;

};