#include "Menu.h"

Menu::Menu() { //Constructor, opens files and builds them from .csv files

	commandFile.open("commands.csv");
    profileFile.open("profiles.csv");

    buildFromFile(commandFile, false, listTerms);
    buildFromFile(profileFile, true, listProfiles);

}
Menu::~Menu() { //Destructor
    
}

//DisplayMenu function, displays the menu and controls user input
void Menu::displayMenu() {
    
    bool game = true; //Check to see if the user hasn't exited
    while (game == true) { //Runs until the user exits the program
        int choice = 0; //Users option
        string toRemove = ""; //For removing commands, initialized here

        while (choice < 1 || choice > 6) { //Until the user exits
            choice = 0; //Reset choice

            cout << "Please Select an Option Listed Below:" << endl << "1. Game Rules" << endl << "2. Play Game" << endl
                << "3. Load Previous Game" << endl << "4. Add Command" << endl << "5. Remove Command" << endl
                << "6. Exit" << endl;

            cin >> choice; //Get input

            if (choice < 1 || choice > 6) { //Error checking
                cout << "That choice is out of range! Try again." << endl;
                choice = 0;
            }
            else {
                //Init the saved profile and name for saved profile outside switch-case
                string name = "";
                Node<double>* savedProfile;

                switch(choice) {
                    case 1: //Option 1: display rules, runs function
                        displayRules();
                    break;
                    case 2: //Option 2: play the game, runs the game loop function with empty username
                        simulateGame("");
                    break;
                    case 3: //Option 3: play the game with previous player
                        cout << "Previous players are: " << endl; //Lists the previous players, prompts the user to select one
                        listProfiles->printList(listProfiles->getHeadPointer());
                        cout << "What is the name of the player you want to play as?" << endl;
                        cin >> name;

                        //Find name and print it out
                        savedProfile = listProfiles->findNode(name, listProfiles->getHeadPointer());
                        cout << "Player was: " << savedProfile->getData1() << endl << "Previous score was: " << savedProfile->getData2() << endl;

                        simulateGame(savedProfile->getData1()); //Simulate game with previous username

                    break;
                    case 4: //Option 4: add a command
                        addCommand(); 
                    break;
                    case 5: //Option 5: remove a command.

                        //Promt the user to remove a command
                        cout << "What is the name of the command you would like to remove from the list?" << endl;
                        cin >> toRemove;

                        listTerms->remove(toRemove, listTerms->getHeadPointer()); //Directly remove the command from the list
                    break;
                    case 6: //Option 6: Exit
                        profileFile.close(); //Closes ifstream and open ofstream to write
                        profileFileOut.open("profiles.csv");

                        //Then writes to the file
                        writeToFile(profileFileOut, listProfiles->getHeadPointer(), 0);

                        //Close output file (for consistency)
                        profileFileOut.close();

                        //Do the same thing as above, but with the other file
                        commandFile.close();
                        commandFileOut.open("commands.csv");

                        writeToFile(commandFileOut, listTerms->getHeadPointer(), 0);

                        commandFileOut.close();
                        game = false; //After files have been written to and streams closed, set the game loop to false.
                    break;
                }
            }
        }
    }
}

//Simulates the game loop
void Menu::simulateGame(string name) {

    //If the game starts with an empty name (option 2), enter a new name here
    if (name == "") {
        cout << "Please provide a username" << endl;
        cin >> name;
    }
    
    //Create a new profile with the name either inputted or used previously
    Node<double>* newProfile = new Node<double>;
    double score = 0;
    newProfile->setData1(name);

    //cNode will travel through the list, giving the definition and right answer 
    Node<string>* cNode = new Node<string>("init", "init");    //Initialize cNode
    cNode->setNextNode(listTerms->getHeadPointer());    //Set it to the head pointer for the list

    //For loop game runs on
    for (int counter = 0; counter < listTerms->getLength(0, listTerms->getHeadPointer()); counter++) {

        //If the next node isn't empty, iterate to it
        if (cNode->getNextNode() != nullptr) {
            cNode = cNode->getNextNode();
        }

        //Display info
        cout << "Current Score is: " << score << endl;
        cout << "Round " << counter + 1 << ":" << endl;
        cout << cNode->getData1() << ":" << endl;

        //Generate the right answer for the question
        int choiceCorrect = rand() % 3 + 1;
        int nextChoice = generateChoice(counter); //Then generate a random definition
        
        if (choiceCorrect == 1) { //If the right answer is option 1
            cout << "1). " << cNode->getData2() << endl; //Output option 1
        
            //Then output randomly chosen option 2
            cout << "2). " << listTerms->findNodeDesc(nextChoice, 0, listTerms->getHeadPointer()) << endl;
            nextChoice = generateChoice(counter); //Find another random definition

            //Then output randomly chosen option 3
            cout << "3). " << listTerms->findNodeDesc(nextChoice, 0, listTerms->getHeadPointer()) << endl;
        }  
        else if (choiceCorrect == 2) { //Same method applies here
            
            cout << "1). " << listTerms->findNodeDesc(nextChoice, 0, listTerms->getHeadPointer()) << endl;
            nextChoice = generateChoice(counter);

            cout << "2). " << cNode->getData2() << endl;

            cout << "3). " << listTerms->findNodeDesc(nextChoice, 0, listTerms->getHeadPointer()) << endl;
        }
        else if (choiceCorrect == 3) { //And here

            cout << "1). " << listTerms->findNodeDesc(nextChoice, 0, listTerms->getHeadPointer()) << endl;
            nextChoice = generateChoice(counter);

            cout << "2). " << listTerms->findNodeDesc(nextChoice, 0, listTerms->getHeadPointer()) << endl;
            
            cout << "3). " << cNode->getData2() << endl;
        }

        //Init and reset player's choice
        int playerChoice = 0;
        cin >> playerChoice; //Have them input their choice

        if (playerChoice == choiceCorrect) { //If it's the right choice, say so and increase score by 1
            cout << "Correct! You earn 1 point." << endl << endl;
            score++;
        }
        else { //Otherwise, lower score by 1
            cout << "Incorrect! The right definition of " << cNode->getData1() << " is: " << cNode->getData2() << "." << endl << endl;
            score--;
        }

    }

    //End game
    if(score == listTerms->getLength(0, listTerms->getHeadPointer())) { //If you get a perfect score
        cout << "You won! You got a perfect score. You data will be saved." << endl;
        
    }
    else { //Otherwise
        cout << "You lost... your score was " << score << " out of " << listTerms->getLength(0, listTerms->getHeadPointer()) << ". Your data will be saved." << endl;
   
    }
    //Insert the score into the profile
    newProfile->setData2(score);
    //Then insert the profile into the profiles list
    listProfiles->insert(newProfile, listProfiles->getHeadPointer());

}

//Diplay rules function
void Menu::displayRules() {

    cout << "RULES:" << endl << "There are as many rounds as there are terms. Every round, one of the terms is displayed, with 3 random options. If you choose the right option," <<
    " you gain a point. Otherwise, you lose a point. Your score at the end of the round is then recorded.";

}

//Add commands function
void Menu::addCommand() {

    //Define the new string and definition
    string name = "", def = "";
    cout << "What is the name of the command you would like to add?" << endl;
    //Prompt user for both
    cin >> name;

    cout << "What is the definition for the command you would like to add?" << endl;
    cin >> def;

    //Create a new node with this info
    Node<string>* newNode = new Node<string>(name, def);

    //Insert it into the list
    listTerms->insert(newNode, listTerms->getHeadPointer());

}

//Randomly generates a choice
int Menu::generateChoice(int counter) {
        int nextChoice = 0; //Init nextChoice
        
        while (nextChoice == 0) { //While nextChoice is 0, randomly get a new number
            //Random get a new number
            nextChoice = rand() % listTerms->getLength(0, listTerms->getHeadPointer()) + 1;

            //If the nextChoice is the same option as the right answer, reset to 0 (reassigns number)
            if (nextChoice == counter) {
                nextChoice = 0;
            }
        }

        //Return final choice
        return nextChoice;
}

//Overloaded buildFromFile functions
//This one is designed for strings
void Menu::buildFromFile(std::ifstream &file, bool type, List<string>* List) {

    std::string temp = "string";

    std::getline(file, temp); //Get first line of file (dead line)

    while (std::getline(file, temp)) { //While we can get lines from the file
        int counter = 0; //Init variables
        string data1 = "", data2 = "";
        
        if (temp != " ") { //If temp isn't empty (final line, also a dead line)
            while (counter < 2) { 
                //Parses the line
                string delimiter = ","; //Find all , in file
                size_t position =  temp.find(delimiter); //Find the first ,
                string tempSubString = temp.substr(0, position); //Create a substring from 0 to the ,

                //Depending on the counter
                if (counter == 0) {
                    data1 = tempSubString; //Set data1
                }
                else {
                    data2 = tempSubString; //Or data2
                }
                    
                temp.erase(0, position + 1); //Erase the part of the line from 0 to , (makes the next segment of the string from 0 to the next ,)
                counter++; //Iterate counter
            }
            //Create a string node with this data
            Node<string>* newNode = new Node<string>(data1, data2);
            List->insert(newNode, List->getHeadPointer()); //Insert it
            
        }
        
    }

}

//buildFromFile version for doubles
void Menu::buildFromFile(std::ifstream &file, bool type, List<double>* List) {

    //Very similar to the string version
    std::string temp = "string";

    std::getline(file, temp);

    while (std::getline(file, temp)) {
        int counter = 0; //Same inits, but data2 is now a double
        string data1 = "";
        double data2 = 0;
        
        while (counter < 2) {
            string delimiter = ","; //Same way of parsing lines
            size_t position =  temp.find(delimiter);
            string tempSubString = temp.substr(0, position);

            if (counter == 0) {
                data1 = tempSubString;
            }
            else {
                data2 = stoi(tempSubString); //Convert the substring (will just be a number) to an into and convert that int into a double
            }
            
            temp.erase(0, position + 1);
            counter++;
        }
        //Create new data point and insert        
        Node<double>* newNode = new Node<double>(data1, data2);
        List->insert(newNode, List->getHeadPointer());
      
       

    }

}

//Overloaded writeToFile function
//String version
void Menu::writeToFile(std::ofstream &file, Node<string>* cNode, int iterations) {

    //Iterations checks to see if this is the first iteration through writeToFile
    //If it is, input the dead line and iterate iterations
    if (iterations == 0) {
        file << "Command Name,definition," << endl;
        iterations++;
    }
    
    //Otherwise, if the next node is populated, then run it again
    if (cNode->getNextNode() != nullptr) {
        writeToFile(file, cNode->getNextNode(), iterations);
    }

    //Lastly, output the data to the file in a csv format
    file << cNode->getData1() << "," << cNode->getData2() << "," << endl;

}
//Double version - works the exact same
void Menu::writeToFile(std::ofstream &file, Node<double>* cNode, int iterations) {

    if (iterations == 0) {
        file << "Name,0," << endl;
        iterations++;
    }
    
    if (cNode->getNextNode() != nullptr) {
        writeToFile(file, cNode->getNextNode(), iterations);
    }

    file << cNode->getData1() << "," << cNode->getData2() << "," << endl;

}