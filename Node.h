#include <iostream>
#include <string>
#include <fstream>

using std::string;
using std::cout;
using std::endl;
using std::cin;

/*
Node class:
This class creates the nodes used for the singly-linked lists in the program.
It uses a template so that it can be used for both profiles and terms.
data1 will always stay a string, as it stores names. data2 will alternate from string (for holding definitions)
to holding doubles (for scores).

The definitions are held in the header file as the Node is a template.
*/
template <typename T>
class Node {

public:

Node() { //3 types of Node Constructors
    data1 = "";
    data2;
    nodeNext = nullptr;
}
Node(string newData1, T newData2) {
    setData1(newData1);
    setData2(newData2);
    nodeNext = nullptr;
}
Node(string newData1, T newData2, Node<T>* nextNode) { 
    setData1(newData1);
    setData2(newData2);
    nodeNext = nextNode;
}
~Node() { //Destructor
    data1 = "";
    nodeNext = nullptr;
}

string getData1() { //Gets the first datapoint
    return data1;
}
T getData2() { //Gets the second datapoint
    return data2;
}


void setData1(string newData) { //Sets the first datapoint
    data1 = newData;
}
void setData2(T newData) { //Sets the second datapoint
    data2 = newData;
}

Node<T>* getNextNode() { //Gets the next node
    return nodeNext;
}
void setNextNode(Node<T>* newNode) { //Sets the next node
    nodeNext = newNode;
}

private:
string data1; //Either name of term or profile name
T data2; //Either description or profile score
Node<T>* nodeNext; //Pointer to next node

};