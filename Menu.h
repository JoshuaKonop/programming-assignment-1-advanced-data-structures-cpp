#include "List.h"

/*
Menu class controls the game loop and the menu
Also controls the building from and writing to files
*/
class Menu {

public:

Menu(); //Constructor and destructor
~Menu();

//Control menu and game loop
void displayMenu();
void simulateGame(string name);

//For rules and adding commands
void displayRules();
void addCommand();

int generateChoice(int counter); //Function needed for game loop

//Overloaded build and write to file functions
//Either string or double
void buildFromFile(std::ifstream &file, bool type, List<string>* List);
void buildFromFile(std::ifstream &file, bool type, List<double>* List);
void writeToFile(std::ofstream &file, Node<string>* cNode, int iterations);
void writeToFile(std::ofstream &file, Node<double>* cNode, int iterations);

private:

//Lists for terms and profiles defined here
List<string>* listTerms = new List<string>;
List<double>* listProfiles = new List<double>;

//file streams. Using ifstream & ofstream instead of fstream because it was simpler for the time being
std::ifstream commandFile;  
std::ifstream profileFile;
std::ofstream commandFileOut;  
std::ofstream profileFileOut;

};